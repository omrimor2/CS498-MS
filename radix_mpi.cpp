/* radix_mpi.cpp: MPI implementation of radix sort
 *
 * This file is distributed under the University of Illinois Open Source
 * License. See LICENSE for details.
 */

#include <algorithm>
#include <cinttypes>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <limits>
#include <mpi.h>
#include <numeric>
#include <ostream>
#include <random>
#include <unistd.h>
#include <vector>

using std::swap;

struct KV {
	std::int32_t key;
	std::int32_t value;
};

void generate_keys_normal(std::int32_t* vec, std::size_t len, int disp)
{
        std::random_device rd;
        std::mt19937 gen{rd() + disp};
        std::normal_distribution<double> dist{1 << 30, 1 << 20};

	for (std::size_t i = 0; i < len; i++) {
		double val = dist(gen);
		vec[i] = val < 0.0 ? 0 :
		         val > INT32_MAX ? INT32_MAX : std::lround(val);
	}
}

void generate_keys_uniform(std::int32_t* vec, std::size_t len, int disp,
                           std::int32_t max)
{
	std::random_device rd;
	std::mt19937 gen{rd() + disp};
	std::uniform_int_distribution<std::int32_t> dist{0, max};

	for (std::size_t i = 0; i < len; i++)
		vec[i] = dist(gen);
}

/*
 * Array distribution/load balancing algorithm taken from §2.10.1 of
 * Introduction to High Performance Scientific Computing
 * by Victor Eijkhout (https://bitbucket.org/VictorEijkhout/hpc-book-and-course)
 */
inline std::size_t array_start(int rank, std::size_t len, int nproc)
{
	return (rank*len)/nproc;
}

inline std::size_t array_distribute(int rank, std::size_t len, int nproc)
{
	return array_start(rank+1, len, nproc) - array_start(rank, len, nproc);
}

inline int array_rank(std::size_t index, std::size_t len, int nproc)
{
	return (nproc*(index+1) - 1)/len;
}

inline int array_index(std::size_t index, int rank, std::size_t len, int nproc)
{
	return index - array_start(rank, len, nproc);
}

void sort_keys(MPI_Win& window, std::int32_t*& vec, std::size_t len,
               std::size_t array_size, int world_size, int radix, double& time)
{
	MPI_Info window_info = MPI_INFO_NULL;
	MPI_Win put_window = MPI_WIN_NULL;
	MPI_Aint alloc_size = (array_size / world_size) + 1;
	std::int32_t* put_vec = nullptr;
	std::vector<MPI_Request> puts(len);

	const std::int32_t mask = (1ULL << radix) - 1;
	const std::size_t histogram_len = 1 << radix;
	std::vector<std::uint64_t> local(histogram_len);
	std::vector<std::uint64_t> global(histogram_len);
	std::vector<std::uint64_t> prior(histogram_len);

	/* Use same info keys */
	MPI_Win_get_info(window, &window_info);
	/* Allocate same size as window */
	MPI_Win_allocate(alloc_size*sizeof(*put_vec), sizeof(*put_vec),
	                 window_info, MPI_COMM_WORLD, &put_vec, &put_window);

	time = MPI_Wtime();
	for (int r = 0; r < 32/radix; r++) {
		/* Reset local histogram */
		std::fill(local.begin(), local.end(), 0);
		/* Make histogram of bits in keys */
		for (std::size_t i = 0; i < len; i++)
			local[(vec[i] >> (r*radix)) & mask]++;

		/* Compute histogram totals */
		MPI_Allreduce(local.data(), global.data(), local.size(),
		              MPI_UINT64_T, MPI_SUM, MPI_COMM_WORLD);

		/* Compute histogram on prior processes */
		MPI_Exscan(local.data(), prior.data(), local.size(),
		              MPI_UINT64_T, MPI_SUM, MPI_COMM_WORLD);
		
		/* Compute exclusive scan on global array */
		std::size_t sum = 0;
		for (auto& i : global) {
			i += sum;
			swap(i, sum);
		}

		#if 0
		/* std::exclusive_scan() isn't supported by libstdc++ */
		std::exclusive_scan(global.begin(), global.end(),
		                    global.begin(), 0);
		#endif

		/* Compute starting indices */
		for (std::size_t i = 0; i < local.size(); i++)
			local[i] = global[i] + prior[i];

		/* Lock RMA window */
		MPI_Win_lock_all(0, put_window);

		/* Put keys in the correct index */
		for (std::size_t i = 0; i < len; i++) {
			/* Get index for key, increment for next key */
			auto index = local[(vec[i] >> (r*radix)) & mask]++;
			/* Compute rank for index */
			int dest = array_rank(index, array_size, world_size);
			/* Compute local array index */
			MPI_Aint disp = array_index(index, dest, array_size,
			                            world_size);
			/* Put key */
			MPI_Rput(&vec[i], 1, MPI_INT32_T, dest,
			         disp, 1, MPI_INT32_T, put_window, &puts[i]);
		}

		/* Complete puts */
		MPI_Waitall(puts.size(), puts.data(), MPI_STATUSES_IGNORE);
		/* Unlock RMA window */
		MPI_Win_unlock_all(put_window);
		/* Wait for everyone */
		MPI_Barrier(MPI_COMM_WORLD);

		/* Swap new vector into place */
		swap(vec, put_vec);
		swap(window, put_window);
	}
	time = MPI_Wtime() - time;

	/* Get global max time */
	MPI_Allreduce(MPI_IN_PLACE, &time, 1, MPI_DOUBLE,
	              MPI_MAX, MPI_COMM_WORLD);

	/* Free window */
	MPI_Win_free(&put_window);
	/* Free info */
	MPI_Info_free(&window_info);
}

void usage(std::ostream& stream, const char* name)
{
	stream << "usage: " << name << " [-h] [-r radix] [-f file]"
	       << std::endl;
}

int main(int argc, char *argv[])
{
	int rank = MPI_PROC_NULL;
	int world_size = 0;
	std::int32_t* keys = nullptr;
	int radix = 1;
	MPI_Info window_info = MPI_INFO_NULL;
	MPI_Win window = MPI_WIN_NULL;
	int c = '\0';
	std::ostream* stream = &std::cout;
	std::ofstream fstream;

	while((c = getopt(argc, argv, "hr:f:")) != -1) {
		switch (c) {
		case 'h':
			usage(std::cout, argv[0]);
			std::exit(EXIT_SUCCESS);
		case 'r':
			radix = std::stoi(optarg);
			if (32 % radix != 0) {
				std::cerr << argv[0]
				          << ": 32 must be divisible by radix"
				          << std::endl;
				usage(std::cerr, argv[0]);
				std::exit(EXIT_FAILURE);
			}
			break;
		case 'f':
			fstream = std::move(std::ofstream(optarg,
			                                  std::ios::app));
			stream = &fstream;
			break;
		case '?':
			usage(std::cerr, argv[0]);
			std::exit(EXIT_FAILURE);
		default:
			std::abort();
		}
	}

	/* Initialize */
	MPI_Init(nullptr, nullptr);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	/* Maybe greater performance by setting these info keys */
	MPI_Info_create(&window_info);
	MPI_Info_set(window_info, "same_size", "true");
	MPI_Info_set(window_info, "same_disp_unit", "true");

	if (rank == 0) {
		/* Ensure that entire time is printed */
		stream->precision(std::numeric_limits<double>::max_digits10);
		/* If using std::cout or at beginning of file, write header */
		if (stream == &std::cout || stream->tellp() == 0) {
			*stream << "N\tDistribution\tProcesses\tRadix\tTime"
			        << std::endl;
		}
	}

	for (std::size_t k = 16; k <= 24; k++) {
		double time = 0.0;
		/* Global array size = 2^k */
		std::size_t array_size = 1 << k;
		/* How many entries do I have? */
		std::size_t local_array_size =
		                array_distribute(rank, array_size, world_size);
		/* All processes allocate same size
		 * Might be larger than local_array_size */
		MPI_Aint alloc_size = (array_size / world_size) + 1;
		MPI_Win_allocate(alloc_size*sizeof(*keys), sizeof(*keys),
		                 window_info, MPI_COMM_WORLD, &keys, &window);

		/* Generate uniformly distributed keys between 0 and 2^31 - 1 */
		generate_keys_uniform(keys, local_array_size, rank, INT32_MAX);
		/* Sort keys */
		sort_keys(window, keys, local_array_size,
		          array_size, world_size, radix, time);
		if (rank == 0) {
			*stream << array_size << "\tA\t" << world_size
			        << "\t" << radix << "\t" << time << std::endl;
		}

		/* Generate normally distrubted keys with 
		 * mean = 2^30, stddev = 2^20 */
		generate_keys_normal(keys, local_array_size, rank);
		/* Sort keys */
		sort_keys(window, keys, local_array_size,
		          array_size, world_size, radix, time);
		if (rank == 0) {
			*stream << array_size << "\tB\t" << world_size
			        << "\t" << radix << "\t" << time << std::endl;
		}

		/* Generate uniformly distributed keys between 0 and 255 */
		generate_keys_uniform(keys, local_array_size, rank, 255);
		/* Sort keys */
		sort_keys(window, keys, local_array_size,
		          array_size, world_size, radix, time);
		if (rank == 0) {
			*stream << array_size << "\tC\t" << world_size
			        << "\t" << radix << "\t" << time << std::endl;
		}

		/* Free window */
		MPI_Win_free(&window);
		keys = nullptr;
	}

	/* Free info */
	MPI_Info_free(&window_info);

	/* Finalize */
	MPI_Finalize();
	/* Exit */
	std::exit(EXIT_SUCCESS);
}
