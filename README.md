CS498-MS Project - Radix Sort with MPI and OpenMP
=================================================

### Requirements
* cmake 3.1+
* Compiler with C++11 and OpenMP support
* MPI-3.0 implementation

### Compilation
```shell
mkdir build
cd build
cmake ..
make
```

The cmake command can be supplied options to determine the build type
(optimization level), compiler flags, and compiler.

### Datasets
There are three datasets generated:
* A. uniformly distributed between $`0`$ and $`2^{31} - 1`$
* B. normally distributed with a mean of $`2^{30}`$
     and a standard deviation of $`2^{20}`$
* C. uniformly distributed between $`0`$ and $`255`$

For each of these, the array size is varied between $`2^{16}`$ and $`2^{24}`$.

The data directory contains output tables from single-node runs
of both the MPI and OpenMP versions, scaling from 1 to 16 processors
and varying the radix between 1 and 16 (a system with a very large memory
could use a radix of 32, though this is not recommended).
Thus there are a total of 675 data points for each version.
