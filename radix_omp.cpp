/* radix_omp.cpp: OpenMP implementation of radix sort
 *
 * This file is distributed under the University of Illinois Open Source
 * License. See LICENSE for details.
 */

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <omp.h>
#include <ostream>
#include <random>
#include <unistd.h>
#include <vector>

using std::swap;

void generate_keys_normal(std::vector<std::int32_t>& vec)
{
	#pragma omp parallel
	{
	        std::random_device rd;
       		std::mt19937 gen{rd() + omp_get_thread_num()};
	        std::normal_distribution<double> dist{1 << 30, 1 << 20};

		#pragma omp for schedule(static)
		for (auto it = vec.begin(); it < vec.end(); ++it) {
			double val = dist(gen);
			*it = val < 0.0 ? 0 :
			      val > INT32_MAX ? INT32_MAX : std::lround(val);
		}
	}
}

void generate_keys_uniform(std::vector<std::int32_t>& vec, std::int32_t max)
{
	#pragma omp parallel
	{
		std::random_device rd;
		std::mt19937 gen{rd() + omp_get_thread_num()};
		std::uniform_int_distribution<std::int32_t> dist{0, max};

		#pragma omp for schedule(static)
		for (auto it = vec.begin(); it < vec.end(); ++it) {
			*it = dist(gen);
		}
	}
}

void sort_keys(std::vector<std::int32_t>& keys, int radix, double& time)
{
	std::vector<std::int32_t> new_keys(keys.size());
	const std::int32_t mask = (1ULL << radix) - 1;
	const std::size_t hist_len = 1 << radix;
	std::vector<std::size_t> global(hist_len);

	time = omp_get_wtime();

	#pragma omp parallel
	{
		const int num_threads = omp_get_num_threads();
		std::vector<std::size_t> local(hist_len);

		for (int r = 0; r < 32/radix; r++) {
			/* Reset global histogram on a single thread */
			#pragma omp single nowait
			std::fill(global.begin(), global.end(), 0);

			/* Reset local histogram */
			std::fill(local.begin(), local.end(), 0);

			/* Fill local histogram */
			#pragma omp for schedule(static) nowait
			for (auto it = keys.begin(); it < keys.end(); ++it) {
				local[(*it >> (r*radix)) & mask]++;
			}

			/* Loop over each thread in order */
			#pragma omp for schedule(monotonic : static, 1) ordered
			for (int t = 0; t < num_threads; t++) {
				/* Compute reduction and exclusive scan  */
				#pragma omp ordered
				for (std::size_t i = 0; i < hist_len; i++) {
					swap(global[i], local[i]);
					global[i] += local[i];
				}
			}

			/* Exclusive scan on global histogram */
			#pragma omp single
			{
				std::size_t sum = 0;
				for (auto& g : global) {
					g += sum;
					swap(g, sum);
				}
			}

			/* Compute indices */
			for (std::size_t i = 0; i < hist_len; i++) {
				local[i] += global[i];
			}

			/* Put keys at correct index */
			#pragma omp for schedule(static)
			for (auto it = keys.begin(); it < keys.end(); ++it) {
				auto index = local[(*it >> (r*radix)) & mask]++;
				new_keys[index] = *it;
			}

			/* Swap into place */
			#pragma omp single
			swap(keys, new_keys);
		}
	}

	time = omp_get_wtime() - time;
}

void usage(std::ostream& stream, const char* name)
{
	stream << "usage: " << name << " [-h] [-r radix] [-f file]"
	       << std::endl;
}

int main(int argc, char *argv[])
{
	std::vector<std::int32_t> keys;
	int radix = 1;
	int c = '\0';
	std::ostream* stream = &std::cout;
	std::ofstream fstream;

	while((c = getopt(argc, argv, "hr:f:")) != -1) {
		switch (c) {
		case 'h':
			usage(std::cout, argv[0]);
			std::exit(EXIT_SUCCESS);
		case 'r':
			radix = std::stoi(optarg);
			if (32 % radix != 0) {
				std::cerr << argv[0]
				          << ": 32 must be divisible by radix"
				          << std::endl;
				usage(std::cerr, argv[0]);
				std::exit(EXIT_FAILURE);
			}
			break;
		case 'f':
			fstream = std::move(std::ofstream(optarg,
			                                  std::ios::app));
			stream = &fstream;
			break;
		case '?':
			usage(std::cerr, argv[0]);
			std::exit(EXIT_FAILURE);
		default:
			std::abort();
		}
	}

	/* Ensure that entire time is printed */
	stream->precision(std::numeric_limits<double>::max_digits10);
	/* If using std::cout or at beginning of file, write header */
	if (stream == &std::cout || stream->tellp() == 0) {
		*stream << "N\tDistribution\tProcesses\tRadix\tTime"
		        << std::endl;
	}

	for (std::size_t k = 16; k <= 24; k++) {
		double time = 0.0;
		std::size_t array_size = 1 << k;
		keys.resize(array_size);

		generate_keys_uniform(keys, INT32_MAX);
		sort_keys(keys, radix, time);
		*stream << array_size << "\tA\t" << omp_get_max_threads()
		        << "\t" << radix << "\t" << time << std::endl;

		generate_keys_normal(keys);
		sort_keys(keys, radix, time);
		*stream << array_size << "\tB\t" << omp_get_max_threads()
		        << "\t" << radix << "\t" << time << std::endl;

		generate_keys_uniform(keys, 255);
		sort_keys(keys, radix, time);
		*stream << array_size << "\tC\t" << omp_get_max_threads()
		        << "\t" << radix << "\t" << time << std::endl;
	}

	std::exit(EXIT_SUCCESS);
}
